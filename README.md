# Flutter Image Upload Example

## Upload Controller

```
class FileUploadController extends APIBaseController
{
    public function fileUpload(Request $request)
    {
        if ($request->hasFile('images')) {
            $image_path = [];
            foreach ($request->file('images') as $file) {
                $unique_code = Str::random(5);
                $image = 'invoice-' . $unique_code . time() . '.' . $file->extension();
                $path = 'public/uploads/test' . $request->order_ref;
                $image_path[] = $file->storeAs($path, $image);
            }
        }

        return $this->sendResponse($image_path, 'Testing');
    }
}
```

## Postman Setting

![](postman.jpg)
