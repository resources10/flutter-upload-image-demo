import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class DioExample extends StatefulWidget {
  const DioExample({Key? key}) : super(key: key);

  @override
  _DioExampleState createState() => _DioExampleState();
}

class _DioExampleState extends State<DioExample> {
  String uploadUrl = "http://10.0.2.2/api/file-upload";
  List<XFile>? _imageFileList;

  final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dio File Upload Demo"),
        centerTitle: true,
      ),
      body: TextButton(
        child: const Text("Pick an images"),
        onPressed: () {
          pickImage();
        },
      ),
    );
  }

  Future pickImage() async {
    try {
      final List<XFile>? pickedFileList = await _picker.pickMultiImage();
      if (pickedFileList == null) return;
      final List<MultipartFile> uploadImage = [];

      for (var pickedFile in pickedFileList) {
        MultipartFile file = await MultipartFile.fromFile(
          pickedFile.path,
          filename: pickedFile.name,
        );
        uploadImage.add(file);
      }

      var formData = FormData.fromMap({'images[]': uploadImage.toList()});
      var dio = Dio();
      var response = await dio.post(uploadUrl, data: formData);
    } catch (e) {
      print("Error: $e");
    }

    // final imageTemporary = File(image.path);
  }
}
